import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './components/Dashboard.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: Dashboard
		},
		{
			path: '/customer',
			name: 'customer',
			component: () => import('./components/Customer.vue')
		},
		{
			path: '/product',
			name: 'product',
			component: () => import('./components/Product.vue')
		},
		{
			path: '/sale',
			name: 'sale',
			component: () => import('./components/Sale.vue')
		},
	],
	scrollBehavior() {
		return {x: 0, y: 0};
	}
});