import firebase from 'firebase/app';
import 'firebase/firestore';

firebase.initializeApp({
  apiKey: "AIzaSyBFAY_zHij0_Ye2tdl70WHcv1fbYIUUIhA",
  authDomain: "simple-home-sales.firebaseapp.com",
  databaseURL: "https://simple-home-sales.firebaseio.com",
  projectId: "simple-home-sales",
  storageBucket: "simple-home-sales.appspot.com",
  messagingSenderId: "617598146668",
  appId: "1:617598146668:web:a9a986e7cb8f0e3985a049",
  measurementId: "G-89CK76DN5F"
});

export const db = firebase.firestore();
