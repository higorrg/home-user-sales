import { db } from '../firebase';

const COLLECTION_NAME = 'customer';
const SUB_COLLECTION_NAME = 'sales';

export default class SaleService {

    async getItems(customerId){
        let snapshot = await db.collection(COLLECTION_NAME).doc(customerId).collection(SUB_COLLECTION_NAME).get();
        let items = [];
        snapshot.forEach(doc => {
            let itemWithId = doc.data();
            itemWithId.id = doc.id;
            items.push(itemWithId);
        });
        return items;
    }

    validate(doc){
        return (doc && doc.total);
    }

    async create(customerId,doc){
        if (this.validate(doc)){
            doc.createdAt = new Date();//.toDateString();
            await db.collection(COLLECTION_NAME).doc(customerId).collection(SUB_COLLECTION_NAME).add(doc);
        }
    }
    
    async update(id,doc){
        if (this.validate(doc)){
            await db.collection(COLLECTION_NAME).doc(id).update(doc);
        }
    }

    async delete(id){
        await db.collection(COLLECTION_NAME).doc(id).delete();
    }
}