import { db } from '../firebase';

const COLLECTION_NAME = 'customer';

export default class CustomerService {

    async getItems(){
        let snapshot = await db.collection(COLLECTION_NAME).get();
        let items = [];
        snapshot.forEach(doc => {
            let itemWithId = doc.data();
            itemWithId.id = doc.id;
            items.push(itemWithId);
        });
        return items;
    }

    validate(doc){
        return (doc && doc.name);
    }

    async create(doc){
        if (this.validate(doc)){
            doc.createdAt = new Date();
            await db.collection(COLLECTION_NAME).add(doc);
        }
    }
    
    async update(id,doc){
        if (this.validate(doc)){
            await db.collection(COLLECTION_NAME).doc(id).update(doc);
        }
    }

    async delete(id){
        await db.collection(COLLECTION_NAME).doc(id).delete();
    }
}